# IPEE

![License badge](https://raster.shields.io/badge/license-The%20Unlicense-lightgrey.png "License badge")

![Thumbnail](thumbnail.png?raw=true "Thumbnail")

Shows your IP adress using [icanhazip](https://icanhazip.com/)

Тhis project would also set a good example of JavaScript XMLHttpRequest.

## Demo

See in action on [GitLab pages](https://ethicist.gitlab.io/ipee) or [here](https://ipee.surge.sh/)

## Contributing

Please feel free to submit pull requests.
Bugfixes and simple non-breaking improvements will be accepted without any questions.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the [LICENSE](LICENSE) file or [unlicense.org](https://unlicense.org).
